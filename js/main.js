$(document).ready(function () {
  var table = $(".an-data-table");
  table.anDataTable({
    "columnDefs": [
      { "targets": "no-search", "searchable": false, "ordering": true },
      { "targets": "no-order", "searchable": true, "ordering": false }
    ]
  });

  $(".botonsillo").on("click", function () {alert("sduioh");});
});

(function ( $ ) {
  $.fn.exists = function () {
    return this.length !== 0;
  }

  $.fn.anDataTable = function ( options ) {
    $.fn.anDataTable.table_container = $('<div/>').addClass('an-data-table_container');
    $.fn.anDataTable.defaults = {
      "columnDefs": [
        { "targets": "_all", "searchable": true, "ordering": true }
      ],
      "no-array": false
    };
    var opts = merge_options();

    this.filter("table.an-data-table").each(function () {
      var table_data = {
        container: null,
        headers: [],
        rows: [],
        search_mode: false,
        attributes: {}
      };

      $.each(this.attributes, function () {
        if(this.specified) {
          table_data.attributes[this.name] = this.value;
        }
      });

      init_table($(this), table_data);
      render_search_input(table_data);
      init_header(table_data);
      init_rows(table_data);
      render_table_rework(table_data);
    });

    function init_table (table, table_data) {
      var table_container = $.fn.anDataTable.table_container;
      //Get the table and wrap it with our container
      table.wrap(table_container);
      var current_container = table.closest('div.an-data-table_container');
      table_data.container = current_container;

      //Get the rows from the table
      var tbody = table.find("tbody");
      var rows = tbody.find("tr");

      //Get the headers and store them in an array for later use
      var table_headers = table.find("thead th");
      var headers = [];
      //First, insert the expand icon space in the header
      var expand = {
        index: "expand",
        array_index: "button-expand",
        text: "",
        classes: "an-data-table_expand-header",
        current_order: ""
      };
      expand["def"] = { "targets": "", "searchable": false, "ordering": false };
      headers.push(expand);
      table_headers.each(function (index) {
        var header = {
          index: index,
          array_index: $(this).html(),
          text: $(this).html(),
          classes: $(this).attr("class") || "",
          current_order: ""
        };
        var matching_def = matching_column_def(header, index);
        header["def"] = matching_def;
        headers.push(header);
      });
      table_data.headers = headers;

      //Prepare the array with all the table info, each object has the headers as properties
      for (var i = 0; i < rows.length; i++) {
        var row = $(rows[i]);
        var id = row.data("id");
        var parent_id = row.data("parent") || null;
        var td = row.find("td");
        var row_data = {};

        row_data.id = id;
        row_data.parent_id = parent_id;
        row_data.showing_children = false;
        row_data.visible = true;
        row_data.cols = {};
        for(var j in headers) {
          var header = headers[j];
          var current_cell = $(td[header.index]);

          row_data.cols[header.array_index] = current_cell.html();
        }
        table_data.rows.push(row_data);
      }
    }

    function render_search_input(table_data) {
      var container = table_data.container;
      var headers = table_data.headers;
      var searchable_headers = headers.filter(h => h.def.searchable);
      var rows = table_data.rows;

      var input = $("<input/>").attr("type", "text").addClass("an-data-table_search-input")
      .on('input', function (e) {
        var caller = $(this);
        var value = caller.val();

        var search_query = caller.val().toLowerCase();
        table_data.search_mode = (search_query !== "");
        var search_words = search_query.split(" ");

        for (var i in rows) {
          table_data.rows[i].visible = !table_data.search_mode;
        }

        $.each(rows, function (index) {
          var row = table_data.rows[index];
          table_data.rows[index].showing_children = true;

          for (var i in search_words) {
            var current_word = search_words[i];
            if (current_word === "") {
              continue;
            }

            for (var j in searchable_headers) {
              if (table_data.rows[index].visible) {
                return true;
              }
              var header_index = searchable_headers[j].array_index;
              var cell_content = row.cols[header_index].toLowerCase();

              if (cell_content.includes(current_word)) {
                table_data.rows[index].visible = true;
                break
              } else {
                table_data.rows[index].visible = false;
              }
            }
          }
        });

        render_table_rework(table_data);
        console.log(table_data);
      });

      var search_div = $("<div />").addClass("an-data-table_search-div");
      var search_label = $("<label />").text("Buscar:").addClass("an-data-table_search-label");
      search_div.append(search_label);
      search_div.append(input);
      container.prepend(search_div);
    }

    function init_header(table_data) {
      var headers = table_data.headers;
      var header_row = table_data.container.find("thead tr");

      var expand_header = $("<th />");
      header_row = header_row.prepend(expand_header);

      for (var i = 0; i < headers.length; i++) {
        var header = headers[i];
        var header_text = header.text;
        var $_header = $(header_row.find("th").get(i));
        $_header.addClass("and-data-table_header-cell").attr("data-name", header_text).attr("data-index", i)
        .on("click", function (e) {
          var caller = $(this);
          var header_index = caller.data("index");
          var header_obj = headers[header_index];
          var header_def = header_obj.def;
          var col_to_order = caller.data("name");
          var current_order = caller.data("order");

          if (!header_def["ordering"]) {
            return false;
          }

          if (header_obj.order === "asc") {
            sort_rows_desc(table_data.rows, col_to_order);
            header_obj.order = "desc";
          } else {
            sort_rows_asc(table_data.rows, col_to_order);
            header_obj.order = "asc";
          }

          headers.map(function(h) {
            if (h === headers[header_index]) {
              return h;
            }
            h.order = "";
            return h;
          });

          render_table_rework(table_data);
        });
      }
    }

    function init_rows(table_data) {
      var container = table_data.container;
      var $_table = container.find("table");
      var $_tbody = $_table.find("tbody");

      for (var key in table_data.rows) {
        var row = table_data.rows[key];

        if (table_data.search_mode && !row.visible) {
          continue;
        }

        var $_row = $_tbody.find('tr[data-id="' + row.id + '"]');
        var button_check = $_row.find(".an-data-table_button-expand");
        if (button_check.exists()) {
          continue;
        }
        var button_expand = $("<button/>").html("v").addClass("an-data-table_button-expand")
        .on("click", function (e) {
          if (!table_data.search_mode) {
            handle_expand_click(table_data, $(this));
          }
        });
        $_row.prepend(button_expand);

        if (row.parent_id !== null) {
          $_row.attr("data-parent-id", row.parent_id);
        }
      }
    }

    function render_table_rework(table_data) {
      var container = table_data.container;
      var $_table = container.find("table");
      var thead = $_table.find("thead").removeClass("and-data-table_header").addClass("and-data-table_header");
      var thr = thead.find("tr").removeClass("and-data-table_header-cell").addClass("and-data-table_header-cell");
      var $_tbody = $_table.find("tbody");
      var rows = table_data.rows;

      $_table.removeClass("an-data-table an-data-table_table").addClass("an-data-table_table");
      for (var key in table_data.rows) {
        var row = table_data.rows[key];
        var $_row = $_tbody.find('tr[data-id="' + row.id + '"]').removeClass("an-data-table_row-invisible");

        if (!row.visible) {
          $_row.addClass("an-data-table_row-invisible");
        }

        $_row.detach();
        $_tbody.append($_row);
      }

      $_table.find("tbody tr").each(function () {
        var tr = $(this);
        var id = tr.data("id");
        var row = search(id, "id", table_data.rows);

        if (row.parent_id != null) {
          return true;
        }

        var children = $_table.find('tbody tr[data-parent-id="' + id + '"]');
        children.each(function () {
          var child = $(this);
          var parent_id = child.data("parent-id");

          var table = child.closest("table");
          var parent = $_table.find('tbody tr[data-id="' + parent_id + '"]');

          if (!table_data.search_mode) {
            move_child(parent, child, table, table_data);
          }
        });
      });

      assign_background_classes(table_data);
    }//render_table_rework

    function hide_all_children($_table, table_data, row) {
      $.each(table_data.rows, function (index, value) {
        var current_row = table_data.rows[index];

        if (current_row.parent_id === row.id) {
          table_data.rows[index].showing_children = false;
          $_table.find('tr[data-id="' + current_row.id + '"]').addClass("an-data-table_row-invisible");
          hide_all_children($_table, table_data, current_row);
        }
      });
    }

    function move_child($_parent, $_child, $_table, table_data) {
      var p_id = $_parent.data("id");
      var c_id = $_child.data("id");
      var parent_row_obj = search(p_id, "id", table_data.rows);
      var child_row_obj = search(c_id, "id", table_data.rows);

      var parent_level = $_parent.attr("data-level");
      var level = $_parent.attr("data-level") || 0;
      level++;
      $_child.attr("data-level", level);

      if (!parent_row_obj.showing_children) {
        child_row_obj.showing_children = false;
      }

      $_child.detach();
      $_child.find('.an-data-table_button-expand')
      .on("click", function (e) {
        handle_expand_click(table_data, $(this));
      });
      $_parent.after($_child);

      var new_children = $_table.find('tbody tr[data-parent-id="' + c_id + '"]');
      new_children.each(function () {
        var child = $(this);
        var parent_id = child.data("parent-id");
        var table = child.closest("table");
        var parent = table.find('tbody tr[data-id="' + parent_id + '"]');

        move_child(parent, child, table, table_data);
      });

      if (parent_row_obj.showing_children) {
        $_child.removeClass("an-data-table_row-invisible");
      } else {
        $_child.addClass("an-data-table_row-invisible");
      }
    }

    function handle_expand_click(table_data, caller) {
      var tr = caller.closest("tr");
      var table = tr.closest("table");
      var id = tr.data("id");
      var row = search(id, "id", table_data.rows);

      var children = table.find('tr[data-parent-id="' + id + '"]');
      if (children.exists()) {
        if (row.showing_children) {
          hide_all_children(table, table_data, row);
        } else {
          children.removeClass("an-data-table_row-invisible");
        }
        row.showing_children = !row.showing_children;
      }
      assign_background_classes(table_data);
    }

    function sort_rows_asc(table_rows, property) {
      table_rows.sort(function (a, b) {
        var cols_a = a.cols;
        var cols_b = b.cols;
        return ( cols_a[property] > cols_b[property] ) ? 1 : ( (cols_b[property] > cols_a[property]) ? -1 : 0 )
      });
    }

    function sort_rows_desc(table_rows, property) {
      table_rows.sort(function (a, b) {

        var cols_a = a.cols;
        var cols_b = b.cols;
        return ( cols_b[property] > cols_a[property] ) ? 1 : ( (cols_a[property] > cols_b[property]) ? -1 : 0 )
      });
    }

    function matching_column_def(header, header_index) {
      var column_defs = opts.columnDefs;
      for (var index in column_defs) {
        var col_def = column_defs[index];
        var target = col_def.targets;

        //We get the target_type, currently we support an integer array and a string
        //The array should represent the header index starting from 0
        //the string represents classes
        var target_type = null;
        if (target.constructor === Array) {
          var all_ok = true;
          for (var a_index in target) {
            if ( !$.isNumeric(target[a_index]) ) {
              all_ok = false;
            }
          }

          if (!all_ok) {
            console.error("Invalid target in colDefs");
            return null;
          }
          target_type = "indexes";
        } else  if (target.constructor === String) {
          target_type = "string";
        }

        if (target_type == null) {
          console.error("Couldn't parse target in colDefs");
          return null;
        }

        //With the target type we can pass
        switch (target_type) {
          case "indexes":
          if (target.includes(header_index)) {
            return col_def;
          }
          break;
          case "string":
          if (target === "_all") {
            return col_def;
          }

          var target_strings = target.split(/\s+/);
          var classes_array = header.classes.split(/\s+/);

          var intersection = target_strings.filter(value => -1 !== classes_array.indexOf(value));
          if (intersection.length > 0) {
            return col_def;
          }
          break;
          default:
          console.error("end of switch default", header);
          return null;
        }
      }

      console.error("end of function", header);
      return null;
    }

    function merge_options() {
      var new_options = {};

      for (var key in $.fn.anDataTable.defaults) {
        var option = $.fn.anDataTable.defaults[key];
        if (option.constructor == Array) {
          new_options[key] = $.merge( $.merge([], options[key]), option );
        } else {
          if ( typeof options[key] === 'undefined' || options[key] === null ) {
            new_options[key] = option;
          } else {
            new_options[key] = options[key];
          }
        }
      }

      return new_options;
    }

    function search(value, property, array) {
      for (var index in array) {
        if (array[index][property] === value) {
          return array[index];
        }
      }
    }

    function assign_background_classes(table_data) {
      var table = table_data.container.find("table");
      table.find("tbody tr").removeClass("odd even");
      var rows = table.find("tbody tr:not(.an-data-table_row-invisible)");

      rows.each(function (index) {
        if (index % 2 === 0) {
          $(rows[index]).addClass("even");
        } else {
          $(rows[index]).addClass("odd");
        }
      });
    }

    return this;
  };
}( jQuery ));
